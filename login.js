function entrar(){
	var usuario = document.getElementById("usuario").value;
	var password = document.getElementById("password").value;
	 $.ajax({
        url:   'login.php',
        type:  'post',
        data:  { "usuario": usuario, "password":password }, 
        success:  function (response) { 
                var data = JSON.parse(response);
                console.log(data);
                if(data.ok=="ok") {
                	sessionStorage.setItem("idusuario",data.idusuario);
                	sessionStorage.setItem("nombre",data.nombre);
                	location="persona.html";
                }
            }
        });
}